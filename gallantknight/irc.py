'''
Created Date: Wednesday January 30th 2019
Author: Matthew L.

Last Modified: Wednesday January 30th 2019 2:26:11 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


import asyncio
# from gallantknight import GallantKnight
from config import cfg


class Irc:
    def __init__(self):
        self.reader = self.writer = None
        self.scripts = self.db = None
        self.connected = False

    async def connect(self):
        self.reader, self.writer = await asyncio.open_connection(
            cfg['twitch', 'host'], cfg['twitch', 'port'])
        self.connected = True

    def write(self, message):
        if self.connected:
            self.writer.write((message + "\r\n").encode())

    async def read(self):
        return (await self.reader.readline()).decode().strip()

    def stop(self):
        self.writer.close()
irc = Irc()
