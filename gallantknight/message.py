'''
Created Date: Thursday January 24th 2019
Author: Matthew L.

Last Modified: Thursday January 24th 2019 1:53:11 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


from source import Source


class Message:
    tags = None
    params = []
    msg = None
    source = None
    to = None

    def __init__(self, tags, params, msg):
        self.tags = tags
        self.params = params
        self.source = Source(params[0])
        if len(params) == 3:
            self.to = params[2]
        self.msg = msg

    def __getattribute__(self, name):
        return super().__getattribute__(name)
