'''
Created Date: Thursday January 31st 2019
Author: Matthew L.

Last Modified: Thursday January 31st 2019 12:16:17 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


class Event:
    def __init__(self, type, cause, message):
        """
        Acts as an container object for the event that has occured.
        
        :param type: The type of event specified be the Event constants.
        :param cause: What or who caused the event.
        :param message: The Message object. See gallantknight.message
        for more info.
        """
        self.type = type
        self.cause = cause
        self.message = message