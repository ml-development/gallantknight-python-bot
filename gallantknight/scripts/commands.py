'''
Created Date: Tuesday January 29th 2019
Author: Matthew L.

Last Modified: Tuesday January 29th 2019 6:23:17 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


class Commands:
    def __init__(self):
        """Initialize commands class and command registry."""
        self.registery = {}

    def register(self, parent, cmd: str, callback):
        """
        Registers a command in the command registry.

        :param parent: The parant script class of that  command.
        :param cmd: The string representation of the command.
        :param callback: The physical function to call when the command
        is recieved.
        """
        if parent in self.registery:
            self.registery[parent][cmd] = callback
        else:
            self.registery[parent] = {}
            self.registery[parent][cmd] = callback

    def exists(self, cmd: str):
        """
        Checks to see if the command exists.

        :param cmd: The string representation of the command.
        :return: bool
        """
        for mod in self.registery:
            if cmd in self.registery[mod]:
                return True
        return False

    def call(self, cmd: str, source, args):
        """Look for and run the callback method the script."""
        for mod in self.registery:
            if cmd in self.registery[mod]:
                self.registery[mod][cmd](source, args)
commands = Commands()
