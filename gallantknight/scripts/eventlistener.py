'''
Created Date: Thursday January 31st 2019
Author: Matthew L.

Last Modified: Thursday January 31st 2019 10:23:36 am
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''
from .events import Events


class EventListener:
    def __init__(self):
        """Initialize event listener and the eventtype attribute."""
        self.eventtype = None

    @staticmethod
    def onmessage(func):
        """
        Check to see if it is a message event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.MESSAGE:
                func(args[1])
        return inner1

    @staticmethod
    def onnotice(func):
        """
        Check to see if it is a notice event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.NOTICE:
                func(args[1])
        return inner1

    @staticmethod
    def oncommand(func):
        """
        Check to see if it is a command event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.COMMAND:
                func(args[1])
        return inner1

    @staticmethod
    def onban(func):
        """
        Check to see if it is a ban event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.BAN:
                func(args[1])
        return inner1

    @staticmethod
    def ontimeout(func):
        """
        Check to see if it is a user timeout event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.TIMEOUT:
                func(args[1])
        return inner1

    @staticmethod
    def onjoin(func):
        """
        Check to see if it is a join event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.ONJOIN:
                func(args[1])
        return inner1

    @staticmethod
    def onuserstate(func):
        """
        Check to see if it is a user state event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.USERSTATE:
                func(args[1])
        return inner1

    @staticmethod
    def onroomstate(func):
        """
        Check to see if it is a room state event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.ROOMSTATE:
                func(args[1])
        return inner1

    @staticmethod
    def onpart(func):
        """
        Check to see if it is a part event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.PART:
                func(args[1])
        return inner1

    @staticmethod
    def onclearchat(func):
        """
        Check to see if it is a clear chat event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.CLEARCHAT:
                func(args[1])
        return inner1

    @staticmethod
    def onclearmsg(func):
        """
        Check to see if it is a clear msg event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.CLEARMSG:
                func(args[1])
        return inner1

    @staticmethod
    def onhosttarget(func):
        """
        Check to see if it is a host event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.HOSTTARGET:
                func(args[1])
        return inner1

    @staticmethod
    def onreconnect(func):
        """
        Check to see if it is a reconnect command event. If so call the
        callback function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.RECONNECT:
                func(args[1])
        return inner1

    @staticmethod
    def onusernotice(func):
        """
        Check to see if it is a user notice event. If so call the callback
        function.
        """
        def inner1(*args, **kwargs):
            if eventlistener.eventtype == Events.USERNOTICE:
                func(args[1])
        return inner1

eventlistener = EventListener()
