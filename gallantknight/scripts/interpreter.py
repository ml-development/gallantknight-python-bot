'''
Created Date: Tuesday January 29th 2019
Author: Matthew L.

Last Modified: Tuesday January 29th 2019 3:45:50 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


from pathlib import Path
from importlib import import_module
from config import cfg
from glob import iglob
from contextlib import contextmanager
import scripts
from irc import irc
from console import console
from inspect import isclass
import sys
import os


class Interpreter:
    def __init__(self):
        """Initialize the script interpreter and start loading scripts."""
        self.default_dir = Path(os.getcwd()) / "scripts"
        self.modules = {}
        self.events = scripts.events
        self.commands = scripts.commands
        self.loader()

    def loader(self):
        """Loads the scripts then enables them."""
        if not self.default_dir.exists():
            self.default_dir.mkdir()

        files = os.listdir(self.default_dir)
        console.info("Loading scripts...")
        with temp_syspath(self.default_dir):
            for file in iglob(f'{self.default_dir}/*.py'):
                self.load_py_file(file)
        for mod in self.modules:
            console.info("Enabling: {}".format(mod))
            self.modules[mod].enable()

    def load_py_file(self, file):
        """
        Loads the script file

        :param file: The file to load.
        """
        if cfg['general', 'debug']:
            console.debug("Loading script: {}".format(file))
        # get the filename and strip the extension
        name = os.path.basename(file).split('.', maxsplit=2)[0]
        mods = import_module(name)
        for value in mods.__dict__.values():
            if isclass(value):
                if issubclass(value, scripts.Script) and \
                   value is not scripts.Script:
                    self.modules[name] = value()
                    self.modules[name].load()


@contextmanager
def temp_syspath(abs_path):
    if abs_path not in sys.path:
        sys.path.insert(0, str(abs_path))
        yield
        sys.path.remove(str(abs_path))
    else:
        yield
