'''
Created Date: Tuesday January 29th 2019
Author: Matthew L.

Last Modified: Tuesday January 29th 2019 6:23:48 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


class Events:
    MESSAGE = 1
    NOTICE = 2
    COMMAND = 3
    BAN = 4
    TIMEOUT = 5
    ONJOIN = 6
    USERSTATE = 7
    ROOMSTATE = 8
    ONPART = 9
    CLEARCHAT = 10
    CLEARMSG = 11
    HOSTTARGET = 12
    RECONNECT = 13
    USERNOTICE = 14

    def __init__(self):
        self.methods = {}
        self.event = None

    def set_event(self, type, event):
        from .eventlistener import eventlistener
        eventlistener.eventtype = type
        eventlistener.event = self.event = event

    def register(self, parent, callback):
        if parent in self.methods:
            self.methods[parent][callback] = callback
        else:
            self.methods[parent] = {}
            self.methods[parent][callback] = callback

    def event_call(self):
        for mod in self.methods:
            for callback in self.methods[mod]:
                self.methods[mod][callback](self.event)
events = Events()
