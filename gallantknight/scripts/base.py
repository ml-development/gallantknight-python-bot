'''
Created Date: Wednesday January 30th 2019
Author: Matthew L.

Last Modified: Wednesday January 30th 2019 1:28:06 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''
from config import cfg
from enum import Enum, auto
import json
import configparser
import xml.etree.ElementTree as ET
import os
from irc import irc
from console import console


class Base:
    class FileFormat(Enum):
        JSON = auto()
        TEXT = auto()
        INI = auto()
        XML = auto()

    def load_file(self, file, file_format):
        """
        Load and parse file into a usable object.

        :param file: The name and location.
        :param file_format: The format we will return as.
        :return: data from the file inb the specified file format.
        """
        if os.path.isfile(file):
            with open(file, "r") as myfile:
                data = myfile.readlines()
                if file_format == self.FileFormat.JSON:
                    return json.load(data)
                elif file_format == self.FileFormat.INI:
                    config = configparser.ConfigParser()
                    config.read_file(data)
                    return config
                elif file_format == self.FileFormat.XML:
                    root = ET.fromstring(data)
                    return root
                else:
                    return data

    def message(self, string: str):
        """
        Send a message to the twitch channel.

        :param string: The meassage being sent.
        """
        if cfg['general', 'debug']:
            console.debug(
                '\033[35mSending msg: \033[0m{}'.format(string))
        irc.write("PRIVMSG #{} :{}".format(
            cfg['twitch', 'channel'], string))

    def whisper(self, to: str, string: str):
        """
        Sends a whisper to a user.

        :param to: The person we are sending the whisper to.
        :param string: The message we are sending.
        """
        if cfg['general', 'debug']:
            console.debug(
                '\033[35mSending whisper to {}: \033[0m{}'.format(to, string))
        irc.write("PRIVMSG #jtv :/w {} {}".format(to, string))

    def debug(self, msg: str):
        """Sends debug message to console."""
        console.debug(msg)

    def info(self, msg: str):
        """Sends an info message to console."""
        console.info(msg)

    # Send a error message to console
    def error(self, msg: str):
        """Sends an error message to console."""
        console.error(msg)

    def get_twitch_config(self, attr):
        """
        Gets a twitch config attribute.

        :param attr: The attribute we are getting from the config.
        :return: The value from the config.
        """
        return cfg['twitch', attr]
base = Base()
