'''
Created Date: Tuesday January 29th 2019
Author: Matthew L.

Last Modified: Tuesday January 29th 2019 3:41:11 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''
from console import console
from config import cfg
from irc import irc
from tags import Tags
from message import Message
from protocols import Protocols
from database import Database
import scripts
import asyncio


class GallantKnight:
    def __init__(self):
        """Initialization off the application."""
        self.scripts = scripts.Interpreter()
        irc.scripts = self.scripts
        self.db = Database(self)
        irc.db = self.db
        self.protocols = Protocols()
        self.running = True

    def run(self):
        """Acts as an entry point for the application."""
        
        self.loop = asyncio.get_event_loop()
        self.db.connect()
        self.reader = self.loop.create_task(self.connect_irc())
        self.loop.create_task(console.run())
        self.loop.run_forever()

    def parse_line(self, line):
        """Parses line into a Message object."""
        if line.startswith("PING"):
            if cfg['general', 'debug'] and line:
                console.debug("PONG")
            irc.write(line.replace("PING", "PONG"))
            return

        if line.startswith("@"):
            split_string = line.partition(" :")
            tags = split_string[0]
            irc_msg = split_string[2]
        else:
            tags = ""
            irc_msg = line[1:]

        if ":" in irc_msg:
            split_string = irc_msg.partition(":")
            params = split_string[0].split()
            msg = split_string[2]
        else:
            params = irc_msg.split()
            msg = ""
        message = Message(Tags(tags), params, msg)
        self.protocols.handler(message)

    def stop(self):
        """Stops the application and exits gracefully"""
        console.stop()
        self.db.disconnect()
        if irc.connected:
            irc.stop()
        self.running = False
        
        self.loop.stop()

    async def connect_irc(self):
        """Act as the main application loop."""
        await irc.connect()
        console.write("Sending OAuth Token...")
        irc.write("PASS " + cfg['twitch', 'oauthtoken'])

        console.write("Sending Botname: {}".format(
            cfg['twitch', 'botname']))
        irc.write("NICK " + cfg['twitch', 'botname'])
        while self.running:

            # FIXME: this await makes the loops hang/is a blocker
            data = await irc.read()
            if console.shutdown:
                self.stop()
            if cfg['general', 'debug'] and data:
                console.debug(data)
            if data:
                self.parse_line(data)
