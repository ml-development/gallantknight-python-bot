'''
Created Date: Thursday January 24th 2019
Author: Matthew L.

Last Modified: Thursday January 24th 2019 2:08:27 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


from message import Message
from tags import Tags
from config import cfg
from console import console
from irc import irc
from utils import BotCommands
from scripts.events import Events
from scripts.event import Event


class Protocols:
    def handler(self, message1: Message):
        cmd = message1.params[1]
        if cmd == "376":
            self.handle_376(message1)
        if cmd == "PRIVMSG":
            self.privmsg(message1)

    def respond(self, message):
        if cfg['general', 'debug'] and message:
            console.debug('\033[35mSending: \033[0m' + message)
        irc.write(message)

    def handle_376(self, message):
        self.respond("CAP REQ :twitch.tv/commands")
        self.respond("CAP REQ :twitch.tv/tags")
        self.respond("CAP REQ :twitch.tv/membership")
        console.write("Joining: {}".format(cfg['twitch', 'channel']))
        self.respond("JOIN #" + cfg['twitch', 'channel'])
        event = Event(Events.ONJOIN, cfg['twitch', 'botname'], message)
        irc.scripts.events.set_event(Events.ONJOIN, event)
        irc.scripts.events.event_call()

    def privmsg(self, message):
        prefix = cfg['general', 'prefix']
        commands = BotCommands()
        if "#" in message.to and prefix in message.msg:
            commands.handler(message)
