'''
Console/terminal related methods.

Created Date: Wednesday January 30th 2019
Author: Matthew L.

Last Modified: Wednesday January 30th 2019 2:18:57 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''
import os
import asyncio
import time

import gallantknight
from aioconsole import ainput
from config import cfg
from utils import BotCommands
import sys


class Console:

    def __init__(self):
        """Initializes console and running state of console."""
        self.running = False
        self.shutdown = False

    def debug(self, msg):
        """
        Prints an debug message to terminal.

        :param msg: The message being sent to the terminal.
        """
        if msg:
            print('\033[93mDebug: \033[0m' + msg)

    def info(self, msg):
        """
        Prints an info message to terminal.

        :param msg: The message being sent to the terminal.
        """
        if msg:
            print('\033[92mInfo: \033[0m' + msg)

    def write(self, msg):
        """
        Writes a message to terminal.

        :param msg: The message being sent to the terminal.
        """
        if msg:
            print('\033[35m' + msg + '\033[0m')

    def error(self, msg):
        """
        Prints an error message to terminal.

        :param msg: The message being sent to the terminal.
        """
        if msg:
            print('\033[91m' + msg + '\033[0m')

    def stop(self):
        """Sets console to shutdown state"""
        self.running = False
        self.shutdown = True

    async def run(self):
        """Start console loop to read console commands."""
        self.running = True
        while self.running:
            line = await ainput("")

            if "stop" in line:
                self.stop()
console = Console()
