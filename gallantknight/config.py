'''
Created Date: Saturday January 19th 2019
Author: Matthew L.

Last Modified: Saturday January 19th 2019 6:45:19 am
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


import json
import getpass
import os


class Config:
    data = {}

    def __init__(self):
        """Loads or generates the config."""
        if os.path.isfile('gallantknight.json'):
            with open('gallantknight.json') as json_file:
                self.data = json.load(json_file)
        else:
            print('Seting General Settings:')
            general = {}
            general['debug'] = input('Enable debug mode [false]: ') \
                or "false"
            general['prefix'] = input('Set the command prefix [!]: ') \
                or "!"

            print('\nDatabase Settings:')
            database = {}
            database['type'] = \
                input("Database Type, mysql or sqlite [sqlite]:") \
                or "sqlite"
            if database['type'].lower() == "sqlite":
                database['location'] = \
                    input("Database location [gallantknight.db]:") \
                    or "gallantknight.db"
            elif database['type'].lower() == "mysql":
                database['host'] = input("Database host [localhost]:") \
                    or "localhost"
                database['name'] = input("Database name [gallantknight]:") \
                    or "gallantknight"
                database['username'] = input("Database username [root]:") \
                    or "root"
                database['password'] = \
                    getpass.getpass("Database password []:") or ""
                database['ssl'] = input("Databases Uses SSL [false]") \
                    or "false"
            
            print('\nSetup Twitch Settings:')
            twitch = {}
            twitch['botname'] = input('Botname [GallantKnight]: ') \
                or "GallantNight"
            twitch['host'] = input('Server [irc.chat.twitch.tv]: ') \
                or "irc.chat.twitch.tv"
            twitch['port'] = input('Port [6667]: ') or 6667
            twitch['channel'] = input('Channel [mldevelopment]: ') \
                or "mldevelopment"
            print("""\nThe OAuth token can be obtained by creating a new \
                  account for your bot, then visit \
                  https://twitchapps.com/tmi/ as your bot. Then paste \
                  in bellow. It won't be shown that you have pasted it \
                  for security reasons just incase you showing your \
                  viewers the chat bot.\n""")
            twitch['oauthtoken'] = \
                getpass.getpass('OAuth Token For Bot []: ') or ""
            # TODO: finish doing the client key and secret prompts.
            print("""To be able to access
            """)
            data = {}
            data['general'] = general
            data['db'] = database
            data['twitch'] = twitch
            self.data = data
            self.save()

    def save(self):
        """Opens and writes the config file."""
        with open('gallantknight.json', 'w') as file:
            json.dump(self.data, file, indent=4)

    def __getitem__(self, item):
        """This get and item from the data attribute."""
        node, attr = item
        try:
            return self.data[node][attr]
        except KeyError as identifier:
            print(f'Error: {identifier}')


cfg = Config()
