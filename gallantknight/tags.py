'''
Created Date: Monday January 21st 2019
Author: Matthew L.

Last Modified: Monday January 21st 2019 6:04:54 am
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


class Tags:
    def __init__(self, tags):
        self.tags = {}
        tags = tags[1:].split(";")
        for tag in tags:
            element = tag.partition("=")
            self.tags[element[0]] = element[2]

    def __getitem__(self, name):
        try:
            return self.tags[name]
        except KeyError as identifier:
            print(f'Error: {identifier}')
