'''
Created Date: Wednesday February 6th 2019
Author: Matthew L.

Last Modified: Wednesday February 6th 2019 10:47:42 am
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''
from config import cfg
from console import console
import sqlite3


class Sqlite:
    def __init__(self, parent):
        self.parent = parent
        try:
            self.con = sqlite3.connect(cfg['db', 'location'])
        except sqlite3.Error as e:
            console.error("Database error: %s. Aborting..." % e)
            self.parent.stop()
    
    def disconnect(self):
        if self.con:
            console.info("Database has been unloaded...")
            self.con.close()
