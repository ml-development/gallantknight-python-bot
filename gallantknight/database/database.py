'''
Created Date: Saturday February 2nd 2019
Author: Matthew L.

Last Modified: Saturday February 2nd 2019 5:17:21 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''
from config import cfg
from console import console
from .mysql import Mysql
from .sqlite import Sqlite


class Database:
    def __init__(self, parent):
        self.db = None
        self.parent = parent
        self.connected = False
        self.db_type = cfg['db', 'type']

    def connect(self):
        if self.db_type == "sqlite":
            self.db = Sqlite(self.parent)
            console.info("Database loaded...")
        elif self.db_type == "mysql":
            self.db = Mysql(self.parent)
            console.info("Database connected...")
        else:
            console.error("Unknown database type! Aborting....")
            self.parent.stop()
        self.connected = True

    def disconnect(self):
        self.db.disconnect()
    
    def query(*args, **kwargs):
        print(args)
