'''
Created Date: Thursday January 10th 2019
Author: Matthew L.

Last Modified: Thursday January 10th 2019 12:28:31 pm
Modified By: Matthew L. at <matthewl@mldevelopment.pro>

Copyright (c) 2019 ML Development

This file is part of gallantknight-python-bot.

gallantknight-python-bot is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gallantknight-python-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gallantknight-python-bot.  If not, see
<http://www.gnu.org/licenses/>.
'''


from setuptools import setup, find_packages
setup(
    name='gallantknight',
    description='A twitch python twitch bot',
    url='https://bitbucket.org/ml-development/gallantknight-python-bot',
    author='ML Development',
    author_email='matthewl@mldevelopment.pro',
    contact='Matthew L.',
    version='0.1',
    packages=find_packages(),
    license='GNU GPL 3.0 license',
    long_description=open('README.md').read(),
    entry_points={
        'console_scripts': [
            'gallantknight = gallantknight.__main__:main',
        ]
        }
)
